# Tutorial 1 - Brute Force Attack

This is a simple script to attempt accessing a protected resource using a list of passwords.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have Node.js installed on your machine.

### Installing

1. Clone the repository
2. Run `npm install` to install the dependencies

## Running the script

To run the script, use the following command:

```sh
npm run start
```